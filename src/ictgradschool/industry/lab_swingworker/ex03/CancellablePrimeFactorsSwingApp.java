package ictgradschool.industry.lab_swingworker.ex03;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class CancellablePrimeFactorsSwingApp extends JPanel implements ActionListener {

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;
    private JTextArea _factorValues;  // Component to display the result.
    private JTextField tfN;
    private PrimeFactorisationWorker p;

    private class PrimeFactorisationWorker extends SwingWorker<List<Long>, Void> {


        @Override
        protected List<Long> doInBackground() throws Exception {
            List<Long> list = new ArrayList<>();
            String strN = tfN.getText().trim();
            long n = 0;

            try {
                n = Long.parseLong(strN);
            } catch (NumberFormatException e) {
                System.out.println(e);
            }

            // Start the computation in the Event Dispatch thread.
            for (long i = 2; i * i <= n; i++) {
                if (this.isCancelled()){
                    break;
                }
                // If i is a factor of N, repeatedly divide it out
                while (n % i == 0) {
                    if (this.isCancelled()){
                        break;
                    }
//                    _factorValues.append(i + "\n");
                    list.add(i);
                    n = n / i;
                    System.out.println(n);


                }
                System.out.println(n);
            }

            // if biggest factor occurs only once, n > 1
            if (n > 1) {
//                _factorValues.append(n + "\n");
                list.add(n);
            }
            return list;
        }

        @Override
        protected void done() {
            try {
                List<Long> longList = get();
                for (Long l : longList) {
                    _factorValues.append(l + "\n");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (Exception e) {
              //  e.printStackTrace();
            }
            // Re-enable the Start button.
            _startBtn.setEnabled(true);

            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());

        }
    }

    public CancellablePrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        tfN = new JTextField(20);

        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.
        _startBtn.addActionListener(this);
        _abortBtn.addActionListener(this);

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }

    public void actionPerformed(ActionEvent event) {
        Object source = event.getSource();

        if (source == _startBtn) {
            // Disable the Start button until the result of the calculation is known.
            _startBtn.setEnabled(false);
            _abortBtn.setEnabled(true);
            // Clear any text (prime factors) from the results area.
            _factorValues.setText(null);

            // Set the cursor to busy.
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            p = new PrimeFactorisationWorker();
            p.execute();

        } else {
            _startBtn.setEnabled(true);
            _abortBtn.setEnabled(false);
            tfN.setText(null);
            _factorValues.setText("Abort!");
            try {
                p.cancel(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            p = null;

        }
    }


    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new CancellablePrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

